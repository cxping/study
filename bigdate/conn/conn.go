/**
 * @author  cxp
 * @date  2020/2/16 13:57
 * @Describer
 */

package conn

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
)

func Connect(ctx context.Context, url string, database string, collectionName string) *mongo.Collection {

	opts := options.Client().ApplyURI(url)
	//连接数据库
	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		log.Fatal(err)
	}
	//判断服务是否能用
	if err = client.Ping(ctx, readpref.Primary()); err != nil {
		log.Fatal(err)
	}
	//获取数据库和集合
	collection := client.Database(database).Collection(collectionName)
	return collection
}
