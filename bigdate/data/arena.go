/**
 * @author  cxp
 * @date  2020/2/16 16:56
 * @Describer
 */

package data

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"study/bigdate/conn"
	"study/bigdate/file"
)

type Arena struct {
	Id     string `bson:"_id"`
	RoleId string `bson:"belong_to"`
	Typ    int64  `bson:"type"`
	Value  int64
}

func CreateFileArena(ctx context.Context) {

	data := GetArena(ctx)
	file.Create("D:\\bigdata\\arena_20200203.csv", []string{"role_id", "integral"},
		data)
}

func GetArena(ctx context.Context) [][]string {
	var Arenas = make([]Arena, 0)

	collection := conn.Connect(ctx, "mongodb://localhost:27017", "svc20200203", "1::running_total")

	// 23-25 503 26-28 500 29-31 501 0201-0203 502
	cur, err := collection.Find(ctx, bson.M{"type": 502})
	if err != nil {
		log.Fatal(err)
	}
	for cur.Next(ctx) {
		var result1 Arena
		err := cur.Decode(&result1)
		if err != nil {
			log.Fatal(err)
		}
		Arenas = append(Arenas, result1)
	}
	var data [][]string
	for _, v := range Arenas {
		data = append(data, []string{v.RoleId, fmt.Sprint(v.Value)})
	}
	return data
}
