/**
 * @author  cxp
 * @date  2020/2/16 17:30
 * @Describer
 */

package data

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"study/bigdate/conn"
	"study/bigdate/file"
)

type Knight struct {
	Id                string `bson:"_id"`
	RoleId            string `bson:"role_id"`
	KnightNo          int    `bson:"knight_no"`
	Level             int    // 等级
	Talent            int    //天赋
	PeerageNo         int    `bson:"peerage_no"`          // 爵位
	TalentBook        int    `bson:"talent_book"`         // 天赋加成
	KingdomPowerBonus int    `bson:"kingdom_power_bonus"` // 王国加成
	//Guards Guard  // 副将
}

type Guard struct {
	StudentId  string
	Profession uint  //职业特长
	Shield     int32 //护盾
	Bonus      int64 //副将实力加成
	SeatIndex  int16 //席位序号
}

func GetKnight(ctx context.Context) [][]string {

	var knights = make([]Knight, 0)

	collection := conn.Connect(ctx, "mongodb://localhost:27017", "svc20200203", "1::knight")

	cur, err := collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	for cur.Next(ctx) {
		var res Knight
		err := cur.Decode(&res)
		if err != nil {
			log.Fatal(err)
		}
		knights = append(knights, res)
	}

	var roleKnights = make(map[string][]Knight, 0)
	for _, v1 := range knights {
		var temp = make([]Knight, 0)
		for _, v2 := range knights {
			if v1.RoleId == v2.RoleId {
				temp = append(temp, v2)
			}
		}
		roleKnights[v1.RoleId] = temp
	}

	var data = make([][]string, 0)
	for _, knights := range roleKnights {
		for _, v1 := range knights {
			temp := []string{
				v1.RoleId,
				v1.Id,
				fmt.Sprint(v1.KnightNo),
				fmt.Sprint(v1.Level),
				fmt.Sprint(v1.Talent),
				fmt.Sprint(v1.PeerageNo),
				fmt.Sprint(v1.TalentBook),
				fmt.Sprint(v1.KingdomPowerBonus),
				//fmt.Sprint(v1.Guards),
			}
			data = append(data, temp)
		}
	}

	row := []string{"role_id", "knight_id", "knight_no", "level", "talent", "peerage_no", "talent_book", "kingdom_power_bonus"}
	file.Create("D:\\bigdata\\knight_20200203.csv", row, data)

	return data
}
