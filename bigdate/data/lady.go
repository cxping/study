/**
 * @author  cxp
 * @date  2020/2/16 13:52
 * @Describer
 */

package data

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"study/bigdate/conn"
	"study/bigdate/file"
)

type Lady struct {
	Id       string `bson:"_id"`
	RoleId   string `bson:"role_id"`
	LadyExp  int64  `bson:"lady_exp"`
	Charm    int64
	Intimacy int64
}

//type RoleLady struct {
//	RoleId string
//	Lady Lady
//}

func GetLady(ctx context.Context) [][]string {

	var ladys = make([]Lady, 0)
	collection := conn.Connect(ctx, "mongodb://localhost:27017", "svc20200203", "1::lady")

	cur, err := collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	for cur.Next(ctx) {
		var res Lady
		err := cur.Decode(&res)
		if err != nil {
			log.Fatal(err)
		}
		ladys = append(ladys, res)
	}

	var roleLadys = make(map[string][]Lady, 0)
	for _, v1 := range ladys {
		var temp = make([]Lady, 0)
		for _, v2 := range ladys {
			if v1.RoleId == v2.RoleId {
				temp = append(temp, v2)
			}
		}
		roleLadys[v1.RoleId] = temp
	}

	var data = make([][]string, 0)
	for _, lady := range roleLadys {
		for _, v1 := range lady {
			temp := []string{
				v1.RoleId,
				v1.Id,
				fmt.Sprint(v1.Charm),
				fmt.Sprint(v1.Intimacy),
				fmt.Sprint(v1.LadyExp)}
			data = append(data, temp)
		}
	}

	row := []string{"role_id", "beauty_id", "charm", "intimacy", "exp"}
	file.Create("D:\\bigdata\\lady_20200203.csv", row, data)
	return nil
}
