/**
 * @author  cxp
 * @date  2020/2/16 13:59
 * @Describer
 */

package data

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"study/bigdate/conn"
	"study/bigdate/file"
)

type Role struct {
	Id             string `bson:"_id"`
	Manifesto      string
	HeadPortraitNo int
	Attrs          []Attr
}
type Attr struct {
	Typ   uint
	Value int64
}
type Stage struct {
	Id      string `bson:"_id"`
	Process Process
	RoleId  string `bson:"role_id"`
}
type Process struct {
	Section int
	Article int
	Chapter int
}

type RoleA struct {
	Id         string `bson:"_id"`
	Power      int64
	Lv         int64
	Flowers    int64
	Popularity int64
	Process    Process
}

func CreateFileRole(ctx context.Context) {
	data := GetRole(ctx)
	file.Create("D:\\bigdata\\role_1.csv", []string{"role_id", "power", "lv", "flowers", "popularity", "stage_progress"},
		data)
}

func GetRole(ctx context.Context) [][]string {
	var roles = make([]Role, 0)
	var stages = make([]Stage, 0)
	var roleA = make([]RoleA, 0)

	collection := conn.Connect(ctx, "mongodb://localhost:27017", "svc0123", "1::role")
	collection2 := conn.Connect(ctx, "mongodb://localhost:27017", "svc0123", "1::stage")

	cur, err := collection.Find(ctx, bson.M{})
	cur2, err := collection2.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	for cur.Next(ctx) {
		var result1 Role
		err := cur.Decode(&result1)
		if err != nil {
			log.Fatal(err)
		}
		roles = append(roles, result1)
	}
	for cur2.Next(ctx) {
		var result1 Stage
		err := cur2.Decode(&result1)
		if err != nil {
			log.Fatal(err)
		}
		stages = append(stages, result1)
	}

	for _, v1 := range roles {
		for _, v2 := range stages {
			if v1.Id == v2.RoleId {
				var power, lv, fl, po int64
				for _, val3 := range v1.Attrs {
					if val3.Typ == 2 {
						lv = val3.Value
					}
					if val3.Typ == 5 {
						fl = val3.Value
					}
					if val3.Typ == 6 {
						po = val3.Value
					}
					if val3.Typ == 7 {
						power = val3.Value
					}
				}
				roleA = append(roleA, RoleA{
					Id:         v2.RoleId,
					Power:      power,
					Lv:         lv,
					Flowers:    fl,
					Popularity: po,
					Process:    v2.Process,
				})
			}
		}
	}
	var data [][]string
	for _, v := range roleA {
		data = append(data, []string{
			v.Id,
			fmt.Sprint(v.Power),
			fmt.Sprint(v.Lv),
			fmt.Sprint(v.Flowers),
			fmt.Sprint(v.Popularity),
			fmt.Sprint(v.Process)})
	}
	return data
}
