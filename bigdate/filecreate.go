package main

import (
	"context"
	"study/bigdate/data"
)

//type Role struct {
//	Id             string  `bson:"_id"`
//	Manifesto      string
//	HeadPortraitNo int
//	Attrs          []Attr
//}
//type Attr struct {
//	Typ   uint
//	Value int64
//}
//type Stage struct {
//	Id   string  `bson:"_id"`
//	Process Process
//	RoleId string `bson:"role_id"`
//}
//type Process struct {
//	Section int
//	Article int
//	Chapter int
//}
//
//type RoleA struct {
//	Id             string  `bson:"_id"`
//	Power      int64
//	Lv int64
//	Flowers int64
//	Popularity int64
//	Process Process
//}

func main() {

	ctx := context.Background()
	//data.CreateFileRole(ctx)
	//data.GetLady(ctx)
	//data.CreateFileArena(ctx)
	data.GetKnight(ctx)
}
